package org.tw.battle.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.tw.battle.infrastructure.DatabaseConnectionProvider.createConnection;

/**
 * @author Liu Xia
 */
public class StudentRepository {
    private final ServiceConfiguration configuration;

    public StudentRepository(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public int create(String name) throws Exception {
        try (
            Connection connection = createConnection(configuration);
            PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO `student` (`name`) VALUES (?)",
                Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, name);
            statement.execute();
            final ResultSet keys = statement.getGeneratedKeys();
            keys.next();
            return keys.getInt(1);
        }
    }
}
